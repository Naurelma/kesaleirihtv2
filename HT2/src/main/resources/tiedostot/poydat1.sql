/*
    pöytien luonti
*/
CREATE TABLE paketti(
    pakettiID INTEGER PRIMARY KEY NOT NULL,
    sisalto INTEGER NOT NULL,
    luokkanro INTEGER NOT NULL,
    lahtopaikka INTEGER NOT NULL,
    kohdepaikka INTEGER NOT NULL,

    lahetysaika DATE NOT NULL,
    saapumisaika DATE NOT NULL,

    FOREIGN KEY("sisalto") REFERENCES "esine"("esineid"),
    FOREIGN KEY("luokkanro") REFERENCES "luokka"("luokkanro"),
    FOREIGN KEY("lahtopaikka") REFERENCES "automaatti"("automaattiid"),
    FOREIGN KEY("kohdepaikka") REFERENCES "automaatti"("automaattiid"),
    /*Lähettämättömällä paketilla on -1 */
    CHECK ((lahtopaikka == -1 AND kohdepaikka == -1) OR lahtopaikka != kohdepaikka),
    CHECK (saapumisaika > lahetysaika)
);


CREATE TABLE esine(
    esineid INTEGER PRIMARY KEY NOT NULL,
    nimi VARCHAR(30) NOT NULL,
    sarkyva BOOLEAN NOT NULL,
    paino INTEGER NOT NULL,
    korkeus INTEGER NOT NULL,
    leveys INTEGER NOT NULL,
    syvyys INTEGER NOT NULL,
    CHECK (paino > 0),
    CHECK (korkeus > 0),
    CHECK (leveys > 0),
    CHECK (syvyys > 0)
);

/*
    tarvitsee uuden kentän matka rajoitteeseen
*/
CREATE TABLE luokka(
    luokkanro INTEGER PRIMARY KEY NOT NULL,
    luokkanimi VARCHAR(30) NOT NULL,
    painoraja INTEGER NOT NULL,
    maxKorkeus INTEGER NOT NULL,
    maxLeveys INTEGER NOT NULL,
    maxSyvyys INTEGER NOT NULL,
    matkaRaja REAL NOT NULL,

    CHECK (maxKorkeus > 0),
    CHECK (maxLeveys > 0),
    CHECK (maxSyvyys > 0)
);


CREATE TABLE automaatti(
    automaattiid INTEGER PRIMARY KEY NOT NULL,
    postinumero INTEGER NOT NULL,
    osoite VARCHAR(30) NOT NULL,
    aukioloajat VARCHAR(50) NOT NULL,
    postoffice VARCHAR(50) NOT NULL,
    latitude REAL NOT NULL,
    longitude REAL NOT NULL,

    FOREIGN KEY("postinumero") REFERENCES "paikkakunta"("postinumero")
);


CREATE TABLE paikkakunta(
    postinumero INTEGER PRIMARY KEY NOT NULL,
    postitoimipaikka VARCHAR(20) NOT NULL
);

CREATE VIEW automaatti_taysi as 
    SELECT * FROM automaatti
    INNER JOIN paikkakunta
    ON automaatti.postinumero == paikkakunta.postinumero
; 

/*
    triggereitä tässä vaiheessa,
     mutta niitä ei voi luoda javan avulla
*/
/*
    ALustetaan luokat ja esineet
*/
INSERT INTO luokka
VALUES (1,
        'firstClass',
        20000,
        20,
        30,
        50,
	150) 
;
INSERT INTO luokka
VALUES (2,
        'secondClass',
        10000,
        40,
        30,
        50,
	9999) 
;
INSERT INTO luokka
VALUES (3,
        'thirdClass',
        50000000,
        50,
        50,
        50,
	999999) 
;
-- debug esineet

INSERT INTO esine
VALUES (1,
        'DVD Levy',
        'TRUE',
        1,
        1,
        5,
        10) 
;

INSERT INTO esine
VALUES (2,
        'Lyijykuutio',
        'FALSE',
        306000,
        30,
        30,
        30) 
;

INSERT INTO esine
VALUES (3,
        'Auton rengas',
        'FALSE',
        10000,
        50,
        50,
        10) 
;

INSERT INTO esine
VALUES (4,
        'd',
        'FALSE',
        10,
        5,
        20,
        5) 
;

INSERT INTO esine
VALUES (5,
        'Putkitelevisio',
        'TRUE',
        20000,
        40,
        30,
        50) 
;

INSERT INTO esine
VALUES (6,
        'Kahvikuppi',
        'FALSE',
        100,
        5,
        5,
        5) 
;

INSERT INTO paketti
VALUES ('1',
        '1',
        '1',
        '1',
        '2',
        '2018-06-15 00:00:00.000',
        '2018-06-17 00:00:00.000') 
;

INSERT INTO paketti
VALUES ('2',
        '1',
        '1',
        '1',
        '2',
        '2018-06-15 00:00:00.000',
        '2018-06-17 00:00:00.000') 
;

INSERT INTO paketti
VALUES ('3',
        '2',
        '3',
        '-1',
        '-1',
        '2018-06-15 00:00:00.000',
        '2018-06-17 00:00:00.000') 
;
