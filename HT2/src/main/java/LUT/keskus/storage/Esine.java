/*
 * Tiedosto luotu: Jun 11, 2018
 * 
 * 
 */
package LUT.keskus.storage;

/**
 *
 * @author Niko Aurelma <niko.aurelma@student.lut.fi>
 */
public class Esine {

    private int id;

    private final String nimi;
    private final int massa;

    private final int korkeus;
    private final int leveys;
    private final int syvyys;

    private final boolean särkyvää;

    /**
     *
     * @param id
     * @param nimi
     * @param massa
     * @param korkeus
     * @param leveys
     * @param syvyys
     * @param särkyvää
     */
    public Esine(int id, String nimi, int massa, int korkeus, int leveys, int syvyys, boolean särkyvää) {
        this.id = id;
        this.nimi = nimi;

        this.massa = massa;
        this.korkeus = korkeus;
        this.leveys = leveys;
        this.syvyys = syvyys;
        this.särkyvää = särkyvää;

    }

    /**
     *
     * @return
     */
    public String getNimi() {
        return nimi;
    }

    /**
     *
     * @return
     */
    public int getMassa() {
        return massa;
    }

    /**
     *
     * @return
     */
    public boolean isSärkyvää() {
        return särkyvää;
    }

    /**
     *
     * @return
     */
    public int getKorkeus() {
        return korkeus;
    }

    /**
     *
     * @return
     */
    public int getLeveys() {
        return leveys;
    }

    /**
     *
     * @return
     */
    public int getSyvyys() {
        return syvyys;
    }

    /**
     *
     * @return
     */
    public int getId() {
        return id;
    }

    /**
     *
     * @param id
     */
    public void setId(int id) {
        this.id = id;
    }

    /**
     *
     * @return
     */
    public String esita() {
        return "Esine{" + "id=" + id + ", nimi=" + nimi + ", massa=" + massa + ", korkeus=" + korkeus + ", leveys=" + leveys + ", syvyys=" + syvyys + ", s\u00e4rkyv\u00e4\u00e4=" + särkyvää + '}';
    }

    @Override
    public String toString() {
        return nimi;
    }

}
