/* paketille lisää ehtoja */
CREATE TRIGGER testPaino
BEFORE INSERT ON paketti
WHEN (SELECT paino FROM esine WHERE esineid == NEW.sisalto) >
     (SELECT painoraja FROM luokka WHERE luokkanro == NEW.luokkanro)
BEGIN
    SELECT RAISE(FAIL, "Paino liian suuri");
END;

CREATE TRIGGER testKorkeus
BEFORE INSERT ON paketti
WHEN (SELECT korkeus FROM esine WHERE esineid == NEW.sisalto) >
     (SELECT maxKorkeus FROM luokka WHERE luokkanro == NEW.luokkanro)
BEGIN
    SELECT RAISE(FAIL, "Korkeus liian suuri");
END;

CREATE TRIGGER testLeveys
BEFORE INSERT ON paketti
WHEN (SELECT leveys FROM esine WHERE esineid == NEW.sisalto) >
     (SELECT maxLeveys FROM luokka WHERE luokkanro == NEW.luokkanro)
BEGIN
    SELECT RAISE(FAIL, "Leveys liian suuri");
END;

CREATE TRIGGER testSyvyys
BEFORE INSERT ON paketti
WHEN (SELECT syvyys FROM esine WHERE esineid == NEW.sisalto) >
     (SELECT maxSyvyys FROM luokka WHERE luokkanro == NEW.luokkanro)
BEGIN
    SELECT RAISE(FAIL, "Syvyys liian suuri");
END;
