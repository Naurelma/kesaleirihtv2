/*
 * Tiedosto luotu: Jun 12, 2018
 * 
 * 
 */
package LUT.keskus;

import com.diogonunes.jcdp.color.ColoredPrinter;
import com.diogonunes.jcdp.color.api.Ansi.Attribute;
import com.diogonunes.jcdp.color.api.Ansi.BColor;
import com.diogonunes.jcdp.color.api.Ansi.FColor;

/**
 * Created just to debug with ease (java own logger was not being nice
 *
 * @author Niko Aurelma <niko.aurelma@student.lut.fi>
 */
public class DebugLoki {

    private static final ColoredPrinter cp = new ColoredPrinter.Builder(1, false)
            .build();

    public static void setLvl(int lvl) {
        cp.setLevel(lvl);
    }

    public static void print(Object msg) {
        cp.print(aikaLeima(), Attribute.NONE, FColor.CYAN, BColor.BLACK);
        cp.clear();
        cp.println(msg);
    }

    public static void debug(Object msg, int level) {
        cp.debugPrint(aikaLeima(), level, Attribute.NONE, FColor.GREEN, BColor.BLACK);
        cp.clear();
        cp.debugPrintln(msg, level);
    }

    public static void error(Object msg, Throwable t) {
        cp.errorPrint(aikaLeima(), Attribute.REVERSE, FColor.RED, BColor.BLACK);
        cp.clear();
        cp.errorPrintln(msg);
        cp.errorPrintln(t.getMessage());

    }

    private static String aikaLeima() {
        return "[" + cp.getDateFormatted() + "]: ";
    }

    private DebugLoki() {
    }
}
