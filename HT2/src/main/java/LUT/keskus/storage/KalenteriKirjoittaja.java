/*
 * Tiedosto luotu: Jun 29, 2018
 * 
 * 
 */
package LUT.keskus.storage;

import LUT.keskus.DebugLoki;
import java.awt.Desktop;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import static java.lang.Math.random;
import java.sql.Date;
import java.util.List;

/**
 *
 * @author Niko Aurelma <niko.aurelma@student.lut.fi>
 */
public class KalenteriKirjoittaja {

    private static final String FORMAATTI = "BEGIN:VEVENT\n"
            + "UID:uid%d@example.com\r\n"
            + "DTSTAMP:%sT170000Z\r\n"
            + "DTSTART:%sT160000Z\r\n"
            + "DTEND:%sT170000Z\r\n"
            + "DESCRIPTION:%s\r\n"
            + "SUMMARY:%s: %s\r\n"
            + "END:VEVENT\r\n";

    private static final String FORMAATTI2 = "";

    private static String muunna(Date d) {
        return d.toString().replace("-", "");
    }

    private static String convert(Paketti p) {
        return String.format(FORMAATTI,
                (int) (random() * 1000), // tällä ei ole niin merkitystä 
                muunna(p.getLuomispaiva()),
                muunna(p.getLuomispaiva()),
                muunna(p.getLuomispaiva()),
                p.getLuokka().esita(),
                p.getLuokka().getNimi(),
                p.getSisalto().toString()
        );
    }

    private static String convert(Reitti r) {
        return String.format(FORMAATTI,
                (int) (random() * 1000),
                muunna(r.getLahetysaika()),
                muunna(r.getSaapumisaika()),
                muunna(r.getLahetysaika()),
                r.esitä(),
                r.getLahtopaikka(),
                r.getKohdepaikka()
        );
    }

    /**
     * Create a ics file based on the sent packets and open it in the default
     * program
     *
     * @param paketit
     *
     *
     */
    public static void kirjoita(List<Paketti> paketit) {
        File tied = new File("kalenteri.ics");
        try (BufferedWriter br = new BufferedWriter(new FileWriter(tied))) {
            br.write("BEGIN:VCALENDAR\r\n"
                    + "VERSION:2.0\r\n");
            for (Paketti paketti : paketit) {

                br.write(convert(paketti));
                for (Reitti r : paketti.getKuljettuReitti()) {
                    br.write(convert(r));
                }
            }
            br.write("END:VCALENDAR\r\n");
        } catch (IOException ex) {
            DebugLoki.error("Kalnenterin kirjoituksessa virhe", ex);
        }
        DebugLoki.debug("Avataan kalenteri!", 0);
        //stackoverflowsta ratkaisu ongelmaan
        if (Desktop.isDesktopSupported()) {
            new Thread(new Runnable() {
                @Override
                public void run() {
                    try {
                        Desktop.getDesktop().open(tied);
                    } catch (IOException ex) {
                        DebugLoki.error("File not foud", ex);
                    }
                }
            }).start();
        }
        DebugLoki.debug("kalenteri avattu!", 0);
    }

}
