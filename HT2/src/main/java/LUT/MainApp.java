
/*
 * Tiedosto luotu: Jun 7, 2018
 * 
 * 
 */
package LUT;

import LUT.keskus.DebugLoki;
import javafx.application.Application;
import javafx.application.Platform;
import javafx.event.EventHandler;
import javafx.fxml.FXMLLoader;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.stage.Stage;
import javafx.stage.WindowEvent;

/**
 * Application entry point
 *
 * @author Niko Aurelma <niko.aurelma@student.lut.fi>
 */
public class MainApp extends Application {


    /*Kaikki kirjoittaminen tapahtuu DebugLokin kautta kiitos */
    /**
     *
     * @param args
     */
    public static void main(String args[]) {
        DebugLoki.setLvl(0);
        if (args.length > 0) {
            DebugLoki.setLvl(Integer.parseInt(args[0]));
        }
        //assertsEnabled();

        launch(args);
        System.exit(0);
    }

    @SuppressWarnings("AssertWithSideEffects")
    private static void assertsEnabled() {
        boolean test = false;
        assert test = true;
        if (!test) {
            throw new RuntimeException(
                    "Suorita -ea option kanssa, jotta assertit ovat päällä!");
        }
    }

    /*
        
    TO DO:
    

    infoa paketin luonti ikkunalle

    
    PUUTTUU:
    EasyUML plugin kaavion luomiseen
    
    Kalenteri näkymä tapahtumille.
    
    Testit
     */
    /**
     *
     * @param stage
     * @throws Exception
     */
    @Override
    public void start(Stage stage) throws Exception {
        Parent root;
        root = FXMLLoader.load(getClass()
                .getResource("/fxml/paaikkuna.fxml"));

        Scene scene = new Scene(root);
        stage.setTitle("HT");

        stage.setResizable(false);
        stage.setScene(scene);
        //pääikkunan suljeettua halutaan poistua
        stage.setOnCloseRequest(new EventHandler<WindowEvent>() {
            @Override
            public void handle(WindowEvent t) {
                Platform.exit();
                System.exit(0);
            }
        });
        stage.show();
    }
}
