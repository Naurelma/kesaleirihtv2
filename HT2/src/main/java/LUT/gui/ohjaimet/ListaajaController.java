/*
 * Tiedosto luotu: Jun 7, 2018
 *
 *
 */
package LUT.gui.ohjaimet;

import LUT.keskus.DebugLoki;
import LUT.keskus.automaatit.SmartPostAutomaatti;
import LUT.keskus.storage.Esine;
import LUT.keskus.storage.KalenteriKirjoittaja;
import LUT.keskus.storage.Luokka;
import LUT.keskus.storage.Paketti;
import LUT.keskus.storage.Reitti;
import LUT.keskus.storage.Varasto;
import LUT.tietokanta.TietokantaLuokka;

import java.net.URL;
import java.util.ResourceBundle;
import javafx.beans.value.ObservableValue;
import javafx.collections.FXCollections;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.geometry.Pos;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.control.ComboBox;
import javafx.scene.control.Label;
import javafx.scene.control.TableColumn;
import javafx.scene.control.TableView;
import javafx.scene.control.cell.PropertyValueFactory;
import javafx.scene.input.MouseButton;
import javafx.scene.input.MouseEvent;
import javafx.scene.layout.VBox;
import javafx.stage.Modality;
import javafx.stage.Stage;

/**
 * FXML Controller class
 *
 * @author Niko Aurelma <niko.aurelma@student.lut.fi>
 */
public class ListaajaController implements Initializable {

    @FXML
    private TableView<SmartPostAutomaatti> automaatiLista;

    @FXML
    private TableView<Paketti> pakettiLista1;

    @FXML
    private TableView<Reitti> reittiLista;
    @FXML
    private Label valittuAutomaattiTxt;
    @FXML
    private Label valittuPakettiTxt;
    @FXML
    private Label valittuReittiTxt;

    /**
     * Initializes the controller class.
     *
     * @param url
     * @param rb
     */
    @Override
    public void initialize(URL url, ResourceBundle rb) {

        Mediator.getInstance().registerListaaja(this);

        alustaPöytä(pakettiLista1);
        alustaPöytä(reittiLista);
        alustaPöytä(automaatiLista);

        paivitaKaikki(new ActionEvent());

        pakettiLista1.addEventHandler(MouseEvent.MOUSE_CLICKED, (MouseEvent t) -> {
            if (t.getButton().equals(MouseButton.PRIMARY) && t.getClickCount() > 1) {
                pirraValittuReitti(new ActionEvent());
            }
        });

        reittiLista.addEventHandler(MouseEvent.MOUSE_CLICKED, (MouseEvent t) -> {
            if (t.getButton().equals(MouseButton.PRIMARY) && t.getClickCount() > 1) {
                piirraReittiKartalle(new ActionEvent());
            }
        });

        automaatiLista.addEventHandler(MouseEvent.MOUSE_CLICKED, (MouseEvent t) -> {
            if (t.getButton().equals(MouseButton.PRIMARY) && t.getClickCount() > 1) {
                piirraValittuAutomaatti(new ActionEvent());
            }
        });
        pakettiLista1.getSelectionModel().selectFirst();
        reittiLista.getSelectionModel().selectFirst();
        automaatiLista.getSelectionModel().selectFirst();

        pakettiLista1.getSelectionModel().selectedItemProperty().addListener(
                (ObservableValue<? extends Paketti> ov, Paketti t, Paketti t1) -> {
                    valittuPakettiTxt.setText(t1.toString());
                });
        reittiLista.getSelectionModel().selectedItemProperty().addListener(
                (ObservableValue<? extends Reitti> ov, Reitti t, Reitti t1) -> {
                    valittuReittiTxt.setText(t1.toString());
                });
        automaatiLista.getSelectionModel().selectedItemProperty().addListener(
                (ObservableValue<? extends SmartPostAutomaatti> ov, SmartPostAutomaatti t, SmartPostAutomaatti t1) -> {
                    valittuAutomaattiTxt.setText(t1.toString());
                });

    }

    @FXML
    private void paivitaKaikki(ActionEvent event) {
        DebugLoki.debug("Päivitetään listoja", 3);

        automaatiLista.getItems().clear();
        reittiLista.getItems().clear();
        pakettiLista1.getItems().clear();

        automaatiLista.setItems(
                Varasto.getInstance().getAutomaatit());

        reittiLista.setItems(FXCollections.observableList(TietokantaLuokka
                .getInstance().getKaikkiReitit()));

        pakettiLista1.setItems(Varasto.getInstance().getPaketit());

    }

    private void alustaPöytä(TableView<?> pöytä) {
        /*sarakkeet on nimetty samoin kuin haettavat arvot */
        for (TableColumn column : pöytä.getColumns()) {
            column.setCellValueFactory(
                    new PropertyValueFactory<>(column.getText().toLowerCase()));
        }
    }

    /**
     *
     * @return
     */
    public SmartPostAutomaatti getValittuAutomaatti() {
        return automaatiLista.getSelectionModel().getSelectedItem();
    }

    @FXML
    private void pirraValittuReitti(ActionEvent event) {
        Paketti item = pakettiLista1.getSelectionModel().getSelectedItem();
        if (item == null) {
            return;
        }
        Mediator.getInstance().piirräReitit(item.getKuljettuReitti(), item.getLuokka().getNro());

    }

    @FXML
    private void avaaKalenterissa(ActionEvent event) {

        KalenteriKirjoittaja.kirjoita(pakettiLista1.getItems());

    }

    @FXML
    private void piirraValittuAutomaatti(ActionEvent event) {
        SmartPostAutomaatti selectedItem = automaatiLista.getSelectionModel().getSelectedItem();
        Mediator.getInstance().piirräMerkki(selectedItem);
    }

    @FXML
    private void piirraPakettiKartalle(ActionEvent event) {
        pirraValittuReitti(event);
    }

    private void piirraReittiKartalle(ActionEvent event) {
        Reitti reitti = reittiLista.getSelectionModel().getSelectedItem();
        Paketti paketti = Varasto.getInstance().getPakettiByID(reitti.getPakettiid());
        Mediator.getInstance().piirräReitit(paketti.getKuljettuReitti(), paketti.getLuokka().getNro());
    }

    @FXML
    private void muokkaaValittuaPakettia(ActionEvent event) {
        /*
        Avaa uuden ikkunan paketin tietojen muokkausta varten
       
         */
        Paketti p = pakettiLista1.getSelectionModel().getSelectedItem();

        final Stage dialog = new Stage();
        dialog.initModality(Modality.APPLICATION_MODAL);
        dialog.initOwner(automaatiLista.getScene().getWindow());

        VBox dialogVbox = new VBox(20);
        dialogVbox.setAlignment(Pos.CENTER);
        //dialogVbox.getChildren().add(new Label(s));

        ComboBox<Luokka> luokat = new ComboBox<>(
                FXCollections.observableArrayList(Varasto.getInstance().getKaikkiLuokat()));
        ComboBox<Esine> esineet = new ComboBox<>(
                FXCollections.observableArrayList(Varasto.getInstance().getKaikkiEsineet()));

        luokat.getSelectionModel().select(p.getLuokka());
        esineet.getSelectionModel().select(p.getSisalto());
        Button tall = new Button("Tallenna");
        Button poista = new Button("Poista");

        tall.setOnAction((ActionEvent t) -> {
            if (p.paivita(luokat.getSelectionModel().getSelectedItem(),
                    esineet.getSelectionModel().getSelectedItem())) {
                PaketinLuontiController.avaaDialog("Paketti päivitetty!", tall.getScene().getWindow());
                TietokantaLuokka.getInstance().addPaketti(p);
            } else {
                PaketinLuontiController.avaaDialog("Paketti Päivitys ei onnistunut!", tall.getScene().getWindow());
            }
            tall.getScene().getWindow().hide();
        });

        poista.setOnAction((ActionEvent t) -> {
            TietokantaLuokka.getInstance().poistaPaketti(p);
            tall.getScene().getWindow().hide();
        });

        dialogVbox.getChildren().add(luokat);
        dialogVbox.getChildren().add(esineet);
        dialogVbox.getChildren().add(tall);
        dialogVbox.getChildren().add(poista);

        Scene dialogScene = new Scene(dialogVbox, 300, 200);

        dialog.setResizable(false);
        dialog.setScene(dialogScene);
        dialog.showAndWait();
    }

    @FXML
    private void poistaValittu(ActionEvent event) {
        Reitti selectedItem = reittiLista.getSelectionModel().getSelectedItem();
        TietokantaLuokka.getInstance().poistaReitti(selectedItem);
    }

}
