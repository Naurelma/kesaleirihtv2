/*
 * Tiedosto luotu: Jun 7, 2018
 *
 *
 */
package LUT.gui.ohjaimet;

import LUT.keskus.DebugLoki;
import LUT.keskus.automaatit.SmartPostAutomaatti;
import LUT.keskus.storage.Luokka;
import LUT.keskus.storage.Reitti;
import LUT.keskus.storage.Varasto;
import LUT.tietokanta.TietokantaLuokka;

import com.sun.javafx.scene.control.skin.ComboBoxListViewSkin;

import java.net.URL;
import java.util.List;
import java.util.ResourceBundle;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.ComboBox;
import javafx.scene.input.KeyEvent;
import javafx.scene.web.WebEngine;
import javafx.scene.web.WebView;
import javafx.stage.Stage;

/**
 *
 * @author Niko Aurelma <niko.aurelma@student.lut.fi>
 */
public class KarttaController implements Initializable {

    @FXML
    private WebView kartta;

    private WebEngine engine;
    @FXML
    private ComboBox<String> Kaupungit;

    private SmartPostAutomaatti alkuAuto = null;
    private SmartPostAutomaatti loppuAuto = null;

    /**
     *
     * @param url
     * @param rb
     */
    @Override
    public void initialize(URL url, ResourceBundle rb) {
        // TODO
        Mediator.getInstance().registerKartta(this);
        engine = kartta.getEngine();

        Kaupungit.getItems().addAll(TietokantaLuokka.getInstance().getKaupungit());
        Kaupungit.getSelectionModel().selectFirst();
        engine.load(this.getClass()
                .getResource("/tiedostot/index.html").toString());

    }

    @FXML
    private void tyhjenna(ActionEvent event) {
        DebugLoki.debug("Tyhjennetään reitit", 3);
        engine.executeScript("document.deletePaths()");
    }

    /**
     * Calculate the birds distance between the points
     *
     * @param alku
     * @param loppu
     * @return Distance
     */
    public double laskeEtäisyys(SmartPostAutomaatti alku, SmartPostAutomaatti loppu) {
        String src = String.format(
                "document.laskePituus([%s, %s, %s, %s])",
                alku.getSijainti().getLatStr(),
                alku.getSijainti().getLngStr(),
                loppu.getSijainti().getLatStr(),
                loppu.getSijainti().getLngStr());
        DebugLoki.debug("Suoritettava src:", 4);
        DebugLoki.debug(src, 4);
        Object arvo = engine.executeScript(src);

        if (arvo instanceof Integer) {
            return ((Integer) arvo).doubleValue();
        } else if (arvo instanceof Double) {
            return (Double) arvo;
        } else {
            return 0;
        }
    }

    @FXML
    private void lisaaUusi(ActionEvent event) {
        DebugLoki.debug("Lisätään automaatteja kartalle", 3);
        String vari = "red";
        String item = Kaupungit.getSelectionModel().getSelectedItem();

        for (SmartPostAutomaatti automaatti : Varasto.getInstance()
                .getAutomaattiByKaupunki(item)) {

            String src = String.format(
                    "document.goToLocation('%s %s %s', '%s<br>%s', '%s')",
                    automaatti.getOsoite(),
                    automaatti.getPostinumero(),
                    automaatti.getKaupunki(),
                    automaatti.getPostoffice(),
                    automaatti.getAukioloajat(),
                    vari
            );
            DebugLoki.debug("Suoritetaan scripti:", 4);
            DebugLoki.debug(src, 4);
            engine.executeScript(src);
        }

    }

    public void lisaaUusiMerkki(SmartPostAutomaatti automat) {
        DebugLoki.debug("Lisätään yksi automaatti kartalle", 3);
        ((Stage) kartta.getScene().getWindow()).show();
        kartta.getScene().getWindow().requestFocus();
        String vari = "blue";
        String src = String.format(
                "document.goToLocation('%s %s %s', '%s<br>%s', '%s')",
                automat.getOsoite(),
                automat.getPostinumero(),
                automat.getKaupunki(),
                automat.getPostoffice(),
                automat.getAukioloajat(),
                vari
        );
        DebugLoki.debug("Suoritettave src:", 4);
        DebugLoki.debug(src, 4);
        engine.executeScript(src);
    }

    @FXML
    private void tyhjennaKaikki(ActionEvent event) {
        DebugLoki.debug("Tyhjennetään kartta täysin", 3);
        kartta.getEngine().reload();
    }

    private void valitseAutomaatti(ActionEvent event) {
        SmartPostAutomaatti valittu = Mediator.getInstance().getValittuAutomaatti();

        if (alkuAuto == null) {
            alkuAuto = valittu;
        } else {
            loppuAuto = valittu;
        }

    }

    private void piirra(int nopeus) {
        if (alkuAuto != null && loppuAuto != null) {
            DebugLoki.debug("Piirretään reitti:", 4);
            DebugLoki.debug(alkuAuto.esita(), 4);
            DebugLoki.debug(alkuAuto.esita(), 4);

            String str = String.format(
                    "document.createPath([%s, %s, %s, %s], '%s', %d)",
                    alkuAuto.getSijainti().getLatStr(),
                    alkuAuto.getSijainti().getLngStr(),
                    loppuAuto.getSijainti().getLatStr(),
                    loppuAuto.getSijainti().getLngStr(),
                    "red",
                    nopeus
            );
            lisaaUusiMerkki(alkuAuto);
            lisaaUusiMerkki(loppuAuto);

            DebugLoki.debug("Suoritetaan scripti:", 4);
            DebugLoki.debug(str, 4);
            Double d = (Double) engine.executeScript(str);

            alkuAuto = null;
            loppuAuto = null;
        }

    }

    @FXML
    private void handleKeySearch(KeyEvent event) {

        if (event.getCode().isLetterKey()) {

            for (String i : Kaupungit.getItems()) {

                if (i.toLowerCase().startsWith(event.getText().toLowerCase())) {
                    Kaupungit.getSelectionModel().select(i);
                    ((ComboBoxListViewSkin) Kaupungit.getSkin())
                            .getListView().scrollTo(Kaupungit.getSelectionModel()
                                    .getSelectedIndex());
                    break;
                }

            }
        }
    }

    /**
     *
     * @param alku
     * @param kohde
     * @param luokka
     */
    void piirraKartta(SmartPostAutomaatti alku, SmartPostAutomaatti kohde, Luokka luokka) {
        ((Stage) kartta.getScene().getWindow()).show();
        kartta.getScene().getWindow().requestFocus();
        alkuAuto = alku;
        loppuAuto = kohde;
        piirra(luokka.getNro());
    }

    void piirräReitit(List<Reitti> li, int nopeus) {
        DebugLoki.debug("Piirretään useita reittejä", 3);
        String src = "document.piirraMonta([";
        String tar = "{arr:[%s, %s, %s, %s], nopeus:%d},";

        Varasto varasto = Varasto.getInstance();
        for (Reitti reitti : li) {
            SmartPostAutomaatti lahto = varasto.getAutomaattiByID(reitti.getLahtopaikka());
            SmartPostAutomaatti kohde = varasto.getAutomaattiByID(reitti.getKohdepaikka());
            lisaaUusiMerkki(lahto);
            lisaaUusiMerkki(kohde);

            src += String.format(tar, lahto.getSijainti().getLatStr(), lahto.getSijainti().getLngStr(),
                    lahto.getSijainti().getLatStr(), lahto.getSijainti().getLngStr(), nopeus);
        }
        src += "])";

        DebugLoki.debug("Suoritetaan scripti:", 4);
        DebugLoki.debug(src, 4);
        engine.executeScript(src);

    }

}
