/*
 * Tiedosto luotu: Jun 14, 2018
 * 
 * 
 */
package LUT.keskus.storage;

/**
 *
 * @author Niko Aurelma <niko.aurelma@student.lut.fi>
 */
public class Luokka {

    private int nro;
    private final String nimi;
    private final int painoraja;

    private final int maxKorkeus;
    private final int maxLeveys;
    private final int maxSyvyys;
    private final double matkaRaja;
    private final int aikaKesto;

    /**
     *
     * @param nro
     * @param nimi
     * @param painoraja
     * @param maxKorkeus
     * @param maxLeveys
     * @param maxSyvyys
     * @param matkaraja
     * @param aikaKesto
     */
    public Luokka(int nro, String nimi, int painoraja, int maxKorkeus, int maxLeveys, int maxSyvyys, double matkaraja, int aikaKesto) {
        this.nro = nro;
        this.nimi = nimi;
        this.painoraja = painoraja;
        this.maxKorkeus = maxKorkeus;
        this.maxLeveys = maxLeveys;
        this.maxSyvyys = maxSyvyys;
        this.matkaRaja = matkaraja;
        this.aikaKesto = aikaKesto;
    }

    /**
     *
     * @return
     */
    public int getNro() {
        return nro;
    }

    /**
     *
     * @return
     */
    public int getPainoraja() {
        return painoraja;
    }

    /**
     *
     * @return
     */
    public double getMatkaRaja() {
        return matkaRaja;
    }

    /**
     *
     * @return
     */
    public int getMaxKorkeus() {
        return maxKorkeus;
    }

    /**
     *
     * @return
     */
    public int getMaxLeveys() {
        return maxLeveys;
    }

    /**
     *
     * @return
     */
    public int getMaxSyvyys() {
        return maxSyvyys;
    }

    /**
     *
     * @return
     */
    public String getNimi() {
        return nimi;
    }

    /**
     *
     * @param nro
     */
    public void setNro(int nro) {
        this.nro = nro;
    }

    @Override
    public String toString() {
        return Integer.toString(nro);
    }

    /**
     *
     * @return
     */
    public String esita() {
        return "Luokka{" + "nro=" + nro + ", nimi=" + nimi
                + ", painoraja=" + painoraja
                + ", maxKorkeus=" + maxKorkeus + ", maxLeveys=" + maxLeveys
                + ", maxSyvyys=" + maxSyvyys + '}';
    }

    /**
     * Enforces the limits in a class
     *
     * @param s
     * @return true if the thing can fit
     */
    boolean canContain(Esine s) {
        if (s == null) {
            return false;
        }
        return (maxKorkeus >= s.getKorkeus()
                && maxLeveys >= s.getLeveys()
                && maxSyvyys >= s.getSyvyys()
                && painoraja >= s.getMassa());
    }

    public int getAikaKesto() {
        return aikaKesto;
    }

}
