/*
 * Tiedosto luotu: Jul 18, 2018
 * 
 * 
 */
package LUT.keskus.storage;

import java.sql.Date;

public class Reitti {

    private final int pakettiid;
    private final int lahetysnro;

    private final int lahtopaikka;
    private final int kohdepaikka;
    private final double matka;
    private final Date lahetysaika;
    private final Date saapumisaika;

    public Reitti(
            int paketti, int id,
            int lähtö, int kohde,
            double matka,
            Date lä, Date sa) {

        this.pakettiid = paketti;
        this.lahetysnro = id;
        this.lahtopaikka = lähtö;
        this.kohdepaikka = kohde;
        this.matka = matka;
        this.lahetysaika = lä;
        this.saapumisaika = sa;
    }

    /**
     *
     * @return
     */
    public int getLahtopaikka() {
        return lahtopaikka;
    }

    /**
     *
     * @return
     */
    public int getKohdepaikka() {
        return kohdepaikka;
    }

    /**
     *
     * @return
     */
    public Date getLahetysaika() {
        return lahetysaika;
    }

    /**
     *
     * @return
     */
    public Date getSaapumisaika() {
        return saapumisaika;
    }

    public int getPakettiid() {
        return pakettiid;
    }

    public int getLahetysnro() {
        return lahetysnro;
    }

    public double getMatka() {
        return matka;
    }

    public String esitä() {
        return "Reitti{" + "paketti=" + pakettiid + ", id=" + lahetysnro + ", lahtopaikka=" + lahtopaikka + ", kohdepaikka=" + kohdepaikka + ", matka=" + matka + ", lahetyspaiva=" + lahetysaika + ", saapumispaiva=" + saapumisaika + '}';
    }

    @Override
    public String toString() {
        return "Reitti{" + "pakettiid=" + pakettiid + ", lahetysnro=" + lahetysnro + ", matka=" + matka + '}';
    }

}
