/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package LUT.keskus.storage;

import LUT.keskus.automaatit.SmartPostAutomaatti;
import LUT.tietokanta.TietokantaLuokka;
import java.util.ArrayList;
import java.util.List;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;

/**
 *
 * @author biksu
 */
public class Varasto {

    private final ObservableList<SmartPostAutomaatti> automaatit;
    private final ObservableList<Paketti> paketit;
    private final ObservableList<Esine> esineet;
    private final ObservableList<Luokka> luokat;

    private Varasto() {
        automaatit = FXCollections.observableList(TietokantaLuokka.getInstance().getKaikkiAutomaatit());
        paketit = FXCollections.observableList(TietokantaLuokka.getInstance().getKaikkiPaketit());
        esineet = FXCollections.observableList(TietokantaLuokka.getInstance().getKaikkiEsineet());
        luokat = FXCollections.observableList(TietokantaLuokka.getInstance().getKaikkiLuokat());
    }

    public static Varasto getInstance() {
        return VarastoHolder.INSTANCE;
    }

    public ObservableList<SmartPostAutomaatti> getAutomaatit() {
        return automaatit;
    }

    public List<SmartPostAutomaatti> getAutomaattiByKaupunki(String kaupunki) {
        List<SmartPostAutomaatti> tulos = new ArrayList<>();
        for (SmartPostAutomaatti s : automaatit) {
            if (s.getKaupunki().equalsIgnoreCase(kaupunki)) {
                tulos.add(s);
            }
        }
        return tulos;
    }

    public ObservableList<Paketti> getPaketit() {
        return paketit;
    }

    public SmartPostAutomaatti getAutomaattiByID(int id) {
        for (SmartPostAutomaatti s : automaatit) {
            if (s.getId() == id) {
                return s;
            }
        }
        return null;
    }

    public Paketti getPakettiByID(int pakettiid) {
        for (Paketti s : paketit) {
            if (s.getPakettiid() == pakettiid) {
                return s;
            }
        }
        return null;
    }

    public Luokka getLuokkaByID(int luokkaNro) {
        for (Luokka luokka : luokat) {
            if (luokka.getNro() == luokkaNro) {
                return luokka;
            }
        }
        return null;
    }

    public ObservableList<Luokka> getKaikkiLuokat() {
        return luokat;
    }

    public ObservableList<Esine> getKaikkiEsineet() {
        return esineet;
    }

    public void poistaPaketti(Paketti p) {
        paketit.remove(p);
    }

    public void addPaketti(Paketti paketti) {
        int indexOf = paketit.indexOf(paketti);
        if (indexOf < 0) {
            paketit.add(paketti);
        }
    }

    public void addEsine(Esine esine) {
        int indexOf = esineet.indexOf(esine);
        if (indexOf < 0) {
            esineet.add(esine);
        }
    }

    public void addLuokka(Luokka luokka) {
        int indexOf = luokat.indexOf(luokka);
        if (indexOf < 0) {
            luokat.add(luokka);
        }
    }

    private static class VarastoHolder {

        private static final Varasto INSTANCE = new Varasto();
    }
}
