/*
 * Tiedosto luotu: Jun 8, 2018
 * 
 * 
 */
package LUT.tietokanta;

import LUT.gui.ohjaimet.Mediator;
import LUT.keskus.DebugLoki;
import LUT.keskus.automaatit.DataBuilder;
import LUT.keskus.automaatit.SmartPostAutomaatti;
import LUT.keskus.storage.Esine;
import LUT.keskus.storage.Luokka;
import LUT.keskus.storage.Paketti;
import LUT.keskus.storage.Reitti;
import LUT.keskus.storage.Varasto;
import java.io.BufferedReader;
import java.io.File;
import java.io.IOException;
import java.io.InputStreamReader;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;
import java.util.Locale;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author Niko Aurelma <niko.aurelma@student.lut.fi>
 */
public final class TietokantaLuokka {

    private static TietokantaLuokka instance = null;

    /**
     * Returns the single allowed instance
     *
     * @return instance
     */
    public static TietokantaLuokka getInstance() {
        //return null;
        if (instance == null) {
            instance = new TietokantaLuokka();
        }
        return instance;

    }

    private Connection con = null;

    private final String dbName = "TIMOTEI.sqlite3";

    //Singelton
    private TietokantaLuokka() {
        DebugLoki.debug("Avataan tietokanta", 0);
        try {
            Class.forName("org.sqlite.JDBC");
            con = DriverManager.getConnection(String.format("jdbc:sqlite:%s", dbName));
            Statement stmt = con.createStatement();
            ResultSet rs = stmt.executeQuery("SELECT count(*) FROM automaatti;");
            rs.next();
            int aInt = rs.getInt(1);
            if (aInt < 500) {
                DebugLoki.debug("Tietokanta resetoidaan, koska se ei sisällä tarpeeksi automaatteja", 2);
                reset();
            }
        } catch (ClassNotFoundException e) {
            DebugLoki.error("Driveriä ei löytynyt!", e);
            System.exit(0);
        } catch (SQLException ex) {
            DebugLoki.error("Jokin meni vikaan tietokanta yhteydessä, joten resetoidaan se.", ex);
            reset();
        }
        try {
            con.createStatement().execute("PRAGMA foreign_keys=ON;");
        } catch (SQLException ex) {
            DebugLoki.error("Pragman asettaminen ei onnistunut1", ex);
        }

    }

    /**
     * Return a list of all the SmartPost from the database
     *
     * @return list
     */
    public List<SmartPostAutomaatti> getKaikkiAutomaatit() {
        DebugLoki.debug("Haetaan kaikki automaatit Tietokannasta", 1);
        List<SmartPostAutomaatti> l = new ArrayList<>();
        try (Statement stmt = con.createStatement();
                ResultSet rs = stmt.executeQuery("SELECT * FROM automaatti_taysi;")) {

            addPostFromRS(rs, l);

        } catch (SQLException ex) {
            DebugLoki.error("Automaattien listauksessa ongelma", ex);
        }

        return l;
    }

    /**
     * Returns all the devices in the specified city
     *
     * @param city The city to look into
     * @return list of SmartPost
     */
    List<SmartPostAutomaatti> getAutomaatitByKaupunki(String city) {
        List<SmartPostAutomaatti> l = new ArrayList<>();
        if (city == null || city.isEmpty()) {
            return l;
        }
        String sql = String.format(
                "SELECT * FROM automaatti_taysi "
                + "WHERE postitoimipaikka == '%s';", city);
        try (Statement stmt = con.createStatement();
                ResultSet rs = stmt.executeQuery(sql)) {

            addPostFromRS(rs, l);
        } catch (SQLException ex) {
            DebugLoki.error("Automaatit by kaupunki", ex);
        }
        return l;
    }

    SmartPostAutomaatti getAutomaattiByID(int id) {
        List<SmartPostAutomaatti> l = new ArrayList<>();
        String sql = String.format(
                "SELECT * FROM automaatti_taysi "
                + "WHERE automaattiid == '%d';", id);
        try (Statement stmt = con.createStatement();
                ResultSet rs = stmt.executeQuery(sql)) {

            addPostFromRS(rs, l);

        } catch (SQLException ex) {
            DebugLoki.error("Automaattien haku kaupungin perusteella", ex);
        }
        return l.get(0);
    }

    private void addPostFromRS(ResultSet rs, List l) throws SQLException {
        while (rs.next()) {
            //rs.get___(nimi); 
            l.add(new SmartPostAutomaatti(
                    rs.getInt("automaattiid"),
                    rs.getInt("postinumero"),
                    rs.getString("postitoimipaikka"),
                    rs.getString("osoite"),
                    rs.getString("aukioloajat"),
                    rs.getString("postoffice"),
                    rs.getDouble("latitude"),
                    rs.getDouble("longitude")));

        }

    }

    /**
     * Returns all the cities in the data base.
     *
     * @return list of cities
     */
    public List<String> getKaupungit() {
        DebugLoki.debug("Haetaan kaikki kaupungit Tietokannasta", 1);
        List<String> l = new ArrayList<>();
        try (Statement stmt = con.createStatement();
                ResultSet rs = stmt.executeQuery("SELECT DISTINCT postitoimipaikka FROM paikkakunta ORDER BY postitoimipaikka ;")) {

            while (rs.next()) {
                l.add(rs.getString("postitoimipaikka"));
            }

        } catch (SQLException ex) {
            DebugLoki.error("Get kaupunki", ex);
        }

        return l;
    }

    /**
     * Returns all packets in the database
     *
     * @return
     */
    public List<Paketti> getKaikkiPaketit() {
        System.out.println(Mediator.getInstance().getKäyttäjä());
        DebugLoki.debug("Haetaan kaikki paketit Tietokannasta", 1);
        ArrayList<Paketti> p = new ArrayList<>();
        try (Statement stmt = con.createStatement();
                ResultSet rs = stmt.executeQuery(String.format(
                        "SELECT * FROM paketti "
                        + "INNER JOIN esine ON sisalto == esineid "
                        + "INNER JOIN luokka ON paketti.luokkanro == luokka.luokkanro "
                        + "%s ;",
                        Mediator.getInstance().getKäyttäjä().equals("root")
                        ? "" : "WHERE omistaja == '" + Mediator.getInstance().getKäyttäjä() + "'"
                ))) {

            rsToPaketti(rs, p);

        } catch (SQLException ex) {
            DebugLoki.error("Get kaikki paketit", ex);
        }
        return p;
    }

    Paketti getPakettiByID(int id) {
        DebugLoki.print(id);
        Paketti pak = null;
        try (Statement stmt = con.createStatement();
                ResultSet rs = stmt.executeQuery(String.format(
                        "SELECT * FROM paketti "
                        + "INNER JOIN esine ON sisalto == esineid "
                        + "INNER JOIN luokka ON paketti.luokkanro == luokka.luokkanro "
                        + "WHERE pakettiId == %d %s;",
                        id,
                        Mediator.getInstance().getKäyttäjä().equals("root")
                        ? "" : "AND omistaja == " + Mediator.getInstance().getKäyttäjä()
                ))) {
            rs.next();
            pak = new Paketti(rs.getInt("pakettiid"),
                    new Esine(
                            rs.getInt("esineid"),
                            rs.getString("nimi"),
                            rs.getInt("paino"),
                            rs.getInt("korkeus"),
                            rs.getInt("leveys"),
                            rs.getInt("syvyys"),
                            rs.getBoolean("sarkyva")),
                    new Luokka(
                            rs.getInt("luokkanro"),
                            rs.getString("luokkanimi"),
                            rs.getInt("painoraja"),
                            rs.getInt("maxKorkeus"),
                            rs.getInt("maxLeveys"),
                            rs.getInt("maxSyvyys"),
                            rs.getDouble("matkaRaja"),
                            rs.getInt("aikaKesto")
                    ),
                    rs.getString("omistaja"),
                    rs.getDate("luomispaiva"));
            taytäReitit(pak);
        } catch (Exception ex) {
            DebugLoki.error("Get paketi by id", ex);
        }
        return pak;

    }

    private void rsToPaketti(ResultSet rs, List<Paketti> p) throws SQLException {
        while (rs.next()) {
            try {

                Paketti pak = new Paketti(rs.getInt("pakettiid"),
                        new Esine(
                                rs.getInt("esineid"),
                                rs.getString("nimi"),
                                rs.getInt("paino"),
                                rs.getInt("korkeus"),
                                rs.getInt("leveys"),
                                rs.getInt("syvyys"),
                                rs.getBoolean("sarkyva")),
                        new Luokka(
                                rs.getInt("luokkanro"),
                                rs.getString("luokkanimi"),
                                rs.getInt("painoraja"),
                                rs.getInt("maxKorkeus"),
                                rs.getInt("maxLeveys"),
                                rs.getInt("maxSyvyys"),
                                rs.getDouble("matkaRaja"),
                                rs.getInt("aikaKesto")
                        ),
                        rs.getString("omistaja"),
                        rs.getDate("luomispaiva"));
                taytäReitit(pak);
                p.add(pak);

            } catch (IOException ex) {
                //poistetaan viallinen pakettitieto
                DebugLoki.error("Rs to paketti", ex);
            }

        }
    }

    private void taytäReitit(Paketti p) {
        try (Statement stmt = con.createStatement();
                ResultSet rs = stmt.executeQuery(String.format("SELECT * FROM lahetys "
                        + "WHERE pakettiid =='%d' "
                        + "ORDER BY lahetysnro",
                        p.getPakettiid()))) {
            while (rs.next()) {
                p.addReitti(new Reitti(
                        p.getPakettiid(),
                        rs.getInt("lahetysnro"),
                        rs.getInt("lahtopaikka"),
                        rs.getInt("kohdepaikka"),
                        rs.getDouble("matka"),
                        rs.getDate("lahetysaika"),
                        rs.getDate("saapumisaika")
                ));
            }

        } catch (SQLException ex) {
            DebugLoki.error("Reittien haussa virhe", ex);
        }

    }

    /**
     *
     *
     * @return
     */
    public List<Luokka> getKaikkiLuokat() {
        DebugLoki.debug("Haetaan kaikki luokat Tietokannasta", 1);
        List<Luokka> luokat = new ArrayList<>();

        try (Statement stmt = con.createStatement();
                ResultSet rs = stmt.executeQuery(
                        "SELECT * FROM luokka ;"
                )) {

            while (rs.next()) {
                luokat.add(new Luokka(
                        rs.getInt("luokkanro"),
                        rs.getString("luokkanimi"),
                        rs.getInt("painoraja"),
                        rs.getInt("maxKorkeus"),
                        rs.getInt("maxLeveys"),
                        rs.getInt("maxSyvyys"),
                        rs.getDouble("matkaRaja"),
                        rs.getInt("aikaKesto")
                ));
            }
        } catch (SQLException ex) {
            DebugLoki.error("Get luokka by id", ex);
        }

        return luokat;
    }

    /**
     * Return all the things in the database
     *
     * @return List of things
     */
    public List<Esine> getKaikkiEsineet() {
        DebugLoki.debug("Haetaan kaikki Esineet Tietokannasta", 1);
        List<Esine> e = new ArrayList<>();
        try (Statement stmt = con.createStatement();
                ResultSet rs = stmt.executeQuery(
                        "SELECT * FROM esine;")) {

            rsToEsine(rs, e);

        } catch (SQLException ex) {
            DebugLoki.error("Get kaikki esineet", ex);
        }
        return e;
    }

    private void rsToEsine(ResultSet rs, List<Esine> e) {
        try {
            while (rs.next()) {
                e.add(new Esine(
                        rs.getInt("esineid"),
                        rs.getString("nimi"),
                        rs.getInt("paino"),
                        rs.getInt("korkeus"),
                        rs.getInt("leveys"),
                        rs.getInt("syvyys"),
                        rs.getBoolean("sarkyva")));
            }
        } catch (SQLException ex) {
            Logger.getLogger(TietokantaLuokka.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    public List<Reitti> getKaikkiReitit() {
        List<Reitti> p = new ArrayList<>();
        try (Statement stmt = con.createStatement();
                ResultSet rs = stmt.executeQuery("SELECT * FROM lahetys")) {

            while (rs.next()) {
                p.add(new Reitti(
                        rs.getInt("pakettiID"),
                        rs.getInt("lahetysnro"),
                        rs.getInt("lahtopaikka"),
                        rs.getInt("kohdepaikka"),
                        rs.getInt("matka"),
                        rs.getDate("lahetysaika"),
                        rs.getDate("saapumisaika")));
            }

        } catch (SQLException ex) {
            DebugLoki.error("Kaikkien reittien haussa ongelma", ex);
        }
        return p;
    }

    /**
     * Add the given packet to database. And also its class and content if they
     * are not present yet, otherwise update them
     *
     * @param paketti lisättävä paketti
     */
    public void addPaketti(Paketti paketti) {

        try (Statement stmt = con.createStatement()) {
            addEsine(paketti.getSisalto());
            addLuokka(paketti.getLuokka());
            addReitit(paketti.getKuljettuReitti());

            //paketti on käyttäjän luoma, joten lisätään se loppuun
            if (paketti.getPakettiid() == -1) {
                addPakettiIlmanTarkistuksia(paketti);
            } else {
                paivitaPaketti(paketti);
            }

        } catch (SQLException ex) {
            DebugLoki.debug("Tietokanta:" + paketti.esitä(), 2);
            DebugLoki.error("Add paketti", ex);
        }
        Varasto.getInstance().addPaketti(paketti);
    }

    private void paivitaPaketti(Paketti paketti) {

        try (Statement stmt = con.createStatement()) {

            stmt.executeUpdate(String.format(Locale.UK,
                    "UPDATE paketti "
                    + "SET sisalto='%d',luokkanro='%d', "
                    + "omistaja='%s', "
                    + "luomispaiva='%tF 00:00:00.000'"
                    + "WHERE pakettiid='%d';",
                    paketti.getSisalto().getId(),
                    paketti.getLuokka().getNro(),
                    paketti.getOmistaja(),
                    paketti.getLuomispaiva(),
                    paketti.getPakettiid()
            ));

        } catch (SQLException ex) {
            DebugLoki.error("paivita paketti", ex);
        }
    }

    /*
    Lisätään paketti ilman mitään tarkistuksia.
    Tarkistukset ennen tämän kutsua
     */
    private void addPakettiIlmanTarkistuksia(Paketti paketti) {

        try (Statement stmt = con.createStatement()) {

            stmt.executeUpdate(String.format(Locale.UK,
                    "INSERT INTO paketti (sisalto,luokkanro,omistaja,luomispaiva)"
                    + "VALUES('%d','%d','%s','%tF 00:00:00.000');",
                    paketti.getSisalto().getId(),
                    paketti.getLuokka().getNro(),
                    paketti.getOmistaja(),
                    paketti.getLuomispaiva()
            ));
            ResultSet rs = stmt.executeQuery("SELECT last_insert_rowid();");
            rs.next();
            paketti.setPakettiid(rs.getInt(1));

        } catch (SQLException ex) {
            DebugLoki.debug("Lisäys" + paketti.esitä(), 2);
            DebugLoki.error("Lisäys ei onnistunut (luultavasti oli jo lisätty)", ex);
        }
    }

    /**
     * Add a thing to the database or update old based on the id.
     *
     * @param esine Thing to be added.
     */
    public void addEsine(Esine esine) {
        try (Statement stmt = con.createStatement()) {

            //esine on käyttäjän luoma, joten lisätään se loppuun
            if (esine.getId() == -1) {
                ResultSet rs = stmt.executeQuery("SELECT esineid FROM esine;");
                int max = 0, tmp;
                while (rs.next()) {
                    tmp = rs.getInt("esineid");
                    max = (max > tmp ? max : tmp);
                }
                esine.setId(max + 1);
                addEsineIlmanTarkistuksia(esine);
            } else {
                paivitaEsine(esine);
            }

        } catch (SQLException ex) {
            DebugLoki.error("Add esine", ex);
        }
        Varasto.getInstance().addEsine(esine);

    }

    private void paivitaEsine(Esine esine) {
        try (Statement stmt = con.createStatement()) {

            stmt.executeUpdate(String.format(Locale.UK,
                    "UPDATE esine SET "
                    + "nimi='%s',sarkyva='%b',"
                    + "paino='%d',"
                    + "korkeus='%d',leveys='%d',syvyys='%d' "
                    + "WHERE esineid='%d';",
                    esine.getNimi(),
                    esine.isSärkyvää(),
                    esine.getMassa(),
                    esine.getKorkeus(),
                    esine.getLeveys(),
                    esine.getSyvyys(),
                    esine.getId()
            ));

        } catch (SQLException ex) {
            DebugLoki.error("Paivita esine", ex);
        }
    }

    private void addEsineIlmanTarkistuksia(Esine esine) {
        try (Statement stmt = con.createStatement()) {

            stmt.executeUpdate(String.format(Locale.UK,
                    "INSERT INTO esine "
                    + "VALUES('%d','%s','%b', '%d','%d','%d','%d');",
                    esine.getId(),
                    esine.getNimi(),
                    esine.isSärkyvää(),
                    esine.getMassa(),
                    esine.getKorkeus(),
                    esine.getLeveys(),
                    esine.getSyvyys()
            ));

        } catch (SQLException ex) {
            DebugLoki.debug("Tietokanta: " + esine.esita(), 2);
            DebugLoki.error("Lisäys ei onnistunut (luultavasti oli jo lisätty)", ex);

        }
    }

    /**
     * Add class to the database or update old based on the id.
     *
     * @param luokka
     */
    public void addLuokka(Luokka luokka) {
        try (Statement stmt = con.createStatement()) {

            if (luokka.getNro() == -1) {
                addLuokkaIlmanTarkistuksia(luokka);
            } else {
                paivitaLuokka(luokka);
            }

        } catch (SQLException ex) {
            DebugLoki.error("add luokka", ex);
        }
        Varasto.getInstance().addLuokka(luokka);
    }

    private void addLuokkaIlmanTarkistuksia(Luokka luokka) {
        //puhtaasti lisätään luokka
        try (Statement stmt = con.createStatement()) {

            stmt.executeUpdate(String.format(Locale.UK,
                    "INSERT INTO luokka (luokkanimi, painoraja, maxKorkeus, maxLeveys, maxSyvyys, matkaRaja) "
                    + "VALUES('%s','%d', '%d','%d','%d','%f')",
                    luokka.getNimi(),
                    luokka.getPainoraja(),
                    luokka.getMaxKorkeus(),
                    luokka.getMaxLeveys(),
                    luokka.getMaxSyvyys(),
                    luokka.getMatkaRaja()
            ));

        } catch (SQLException ex) {
            DebugLoki.debug("Tietokanta: " + luokka.esita(), 2);
            DebugLoki.error("Lisäys ei onnistunut (luultavasti oli jo lisätty)", ex);
        }

    }

    private void paivitaLuokka(Luokka luokka) {
        try (Statement stmt = con.createStatement()) {

            stmt.executeUpdate(String.format(Locale.UK,
                    "UPDATE luokka SET "
                    + "luokkanimi='%s', "
                    + "painoraja='%d', "
                    + "maxKorkeus='%d', "
                    + "maxLeveys='%d', "
                    + "maxSyvyys='%d', "
                    + "matkaRaja='%f' "
                    + "WHERE luokkanro='%d' ",
                    luokka.getNimi(),
                    luokka.getPainoraja(),
                    luokka.getMaxKorkeus(),
                    luokka.getMaxLeveys(),
                    luokka.getMaxSyvyys(),
                    luokka.getMatkaRaja(),
                    luokka.getNro()
            ));

        } catch (SQLException ex) {
            DebugLoki.error("Luokan päivittämisessä ongelma", ex);
        }
    }

    private void addReitit(List<Reitti> reitit) {
        try {
            Statement stmt = con.createStatement();
            for (Reitti r : reitit) {
                try {
                    stmt.execute(String.format(
                            "INSERT INTO lahetys VALUES "
                            + "('%d','%d','%d','%d', '%f','%tF 00:00:00.000','%tF 00:00:00.000')",
                            r.getPakettiid(), r.getLahetysnro(),
                            r.getLahtopaikka(), r.getKohdepaikka(),
                            r.getMatka(),
                            r.getLahetysaika(), r.getSaapumisaika()
                    ));

                } catch (SQLException ex) {
                    if (ex.getErrorCode() != 19) {
                        DebugLoki.error("Reitin lisäys ei onnistunut", ex);
                    } else {
                        //19 pitäisi olla unique/ pk virhe
                        DebugLoki.error("Reitti on jo lisätty", ex);
                    }
                }
            }
        } catch (SQLException ex) {
            DebugLoki.error("Reittien lisäys ei onnistunut ollenkaan!", ex);
        }
    }

    /**
     * deletes the database and creates it from scratch
     */
    public void reset() {
        try {
            con.close();
            File f = new File(dbName);
            f.delete();
            con = DriverManager.getConnection(String.format("jdbc:sqlite:%s", dbName));
            luoPöydät();
            //muun luokan vastuulla hakea automaatit
            //mutta tämä luokka lisää ne kantaan
            taytaAutomaatit(DataBuilder.getInstance().getList());

        } catch (SQLException ex) {
            DebugLoki.error("Tietokanta reset", ex);
        }
        DebugLoki.debug("Reset done", 2);
        try {
            con.createStatement().execute("PRAGMA foreign_keys=ON;");
        } catch (SQLException ex) {
            DebugLoki.error("Pragman asettaminen ei onnistunut1", ex);
        }
    }

    private void luoPöydät() {
        DebugLoki.debug("Aloitetaan luonti", 3);
        try (Statement stmt = con.createStatement();
                BufferedReader br = new BufferedReader(new InputStreamReader(
                        this.getClass().getResourceAsStream("/tiedostot/poydat.sql"), "utf8"))) {

            //lue tiedostosta sql lauseet ja suorita ne
            String sql = "";
            String line;
            while ((line = br.readLine()) != null) {
                sql += line + "\n";
                if (sql.contains(";")) {
                    stmt.execute(sql);

                    sql = "";
                }
            }

        } catch (SQLException ex) {
            DebugLoki.error("Pöytien luonnissa virhe", ex);

        } catch (IOException ex) {
            DebugLoki.error("Poydat tiedostoa ei loydy", ex);
            System.exit(12);
        }
        DebugLoki.debug("Pöydät luotu", 3);

    }

    private void taytaAutomaatit(ArrayList<SmartPostAutomaatti> lista) {
        DebugLoki.debug("Aloitetaan automaattien kirjoitus", 3);
        try (Statement stmt = con.createStatement()) {
            con.setAutoCommit(false);
            int koko = lista.size();
            for (SmartPostAutomaatti automaatti : lista) {
                try {
                    String sql = String.format("INSERT INTO paikkakunta VALUES ('%d', '%s');",
                            automaatti.getPostinumero(),
                            automaatti.getKaupunki()
                    );

                    stmt.execute(sql);
                } catch (SQLException ex) {
                    //Useita toistoja samoista paikkakunnista, hiljennetään ne
                    //DebugLoki.debug(ex, 5);
                }

                try {
                    stmt.execute(String.format(
                            "INSERT INTO automaatti (postinumero,"
                            + "osoite, aukioloajat, postoffice, latitude, longitude)"
                            + "VALUES ('%d', '%s', '%s', '%s', '%s', '%s');",
                            automaatti.getPostinumero(),
                            automaatti.getOsoite(),
                            automaatti.getAukioloajat(),
                            automaatti.getPostoffice(),
                            automaatti.getSijainti().getLatStr(),
                            automaatti.getSijainti().getLngStr()
                    ));
                } catch (SQLException ex) {
                    DebugLoki.debug("Tietokanta:" + automaatti, 2);
                    DebugLoki.error("Automaatin lisäys ei onnistunut", ex);
                }

            }
            con.commit();
            con.setAutoCommit(true);

        } catch (SQLException ex) {
            DebugLoki.error("Batch fail", ex);
        }
        DebugLoki.debug("Automaatit täytetty", 3);
    }

    @Override
    protected void finalize() throws Throwable {
        DebugLoki.debug("Suljetaan Tietokanta!", 0);
        con.commit();
        con.close();
        super.finalize();

    }

    public void poistaPaketti(Paketti p) {
        try (Statement stmt = con.createStatement()) {
            stmt.executeUpdate(String.format("DELETE FROM paketti WHERE pakettiID = %d", p.getPakettiid()));
            Varasto.getInstance().poistaPaketti(p);

        } catch (SQLException ex) {
            DebugLoki.error("Poistossa ongelma!", ex);
        }

    }

    public void poistaReitti(Reitti r) {
        try (Statement stmt = con.createStatement()) {
            stmt.executeUpdate(String.format("DELETE FROM lahetys WHERE lahetysnro = %d", r.getLahetysnro()));

        } catch (SQLException ex) {
            DebugLoki.error("Poistossa ongelma!", ex);
        }

    }

    public boolean tarkistaKirjautuminen(String käyttäjä, String hashCode) {
        try (Statement stmt = con.createStatement();
                ResultSet rs = stmt.executeQuery(
                        String.format("SELECT passwd FROM kayttaja WHERE tunnus = '%s';",
                                käyttäjä))) {

            rs.next();
            String aInt = rs.getString(1);
            
            
            return hashCode.equals(aInt);

        } catch (SQLException ex) {
            DebugLoki.error("Tunnus ei löytynyt!", ex);
        }
        return false;
    }

    public boolean addTunnus(String käyttäjä, String hashCode) {
        try (Statement stmt = con.createStatement();) {
            stmt.executeUpdate(
                    String.format("INSERT INTO kayttaja values('%s', '%s');",
                            käyttäjä,
                            hashCode));

        } catch (SQLException ex) {
            DebugLoki.error("Tunnus löytyis!", ex);
            return false;
        }
        return true;

    }

}
