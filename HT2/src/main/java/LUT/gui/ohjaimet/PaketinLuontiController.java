/*
 * Tiedosto luotu: Jun 18, 2018
 * 
 * 
 */
package LUT.gui.ohjaimet;

import LUT.keskus.DebugLoki;
import LUT.keskus.automaatit.SmartPostAutomaatti;
import LUT.keskus.storage.Esine;
import LUT.keskus.storage.Luokka;
import LUT.keskus.storage.Paketti;
import LUT.keskus.storage.Reitti;
import LUT.keskus.storage.Varasto;
import LUT.tietokanta.TietokantaLuokka;
import com.sun.javafx.scene.control.skin.ComboBoxListViewSkin;

import java.io.IOException;
import java.net.URL;
import java.sql.Date;
import java.util.Calendar;
import java.util.List;
import java.util.ResourceBundle;
import javafx.beans.value.ObservableValue;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.geometry.Pos;
import javafx.scene.Node;
import javafx.scene.Scene;
import javafx.scene.control.CheckBox;
import javafx.scene.control.ComboBox;
import javafx.scene.control.Label;
import javafx.scene.control.RadioButton;
import javafx.scene.control.TabPane;
import javafx.scene.control.TextField;
import javafx.scene.control.Toggle;
import javafx.scene.control.ToggleGroup;
import javafx.scene.input.KeyEvent;
import javafx.scene.layout.Pane;
import javafx.scene.layout.VBox;
import javafx.scene.text.Font;
import javafx.stage.Modality;
import javafx.stage.Stage;
import javafx.stage.Window;

/**
 * FXML Controller class
 *
 * @author Niko Aurelma <niko.aurelma@student.lut.fi>
 */
public class PaketinLuontiController implements Initializable {

    @FXML
    private ComboBox<String> lahtoKaupunki;
    @FXML
    private ComboBox<SmartPostAutomaatti> lahtoAutomaatti;
    @FXML
    private ComboBox<String> kohdeKaupunki;
    @FXML
    private ComboBox<SmartPostAutomaatti> kohdeAutomaatti;
    @FXML
    private CheckBox cBoxIsBreakable;
    @FXML
    private TextField esineNimi;
    @FXML
    private TextField korkeus;
    @FXML
    private TextField leveys;
    @FXML
    private TextField syvyys;
    @FXML
    private TextField massa;
    @FXML
    private ComboBox<Esine> esineLista;
    @FXML
    private RadioButton firstclass;
    @FXML
    private RadioButton secondclass;
    @FXML
    private RadioButton thirdclass;
    @FXML
    private Pane infoPanel;
    @FXML
    private Font x1;

    @FXML
    private Label kestoIndikaattori;

    private final ToggleGroup group = new ToggleGroup();
    @FXML
    private Label valitunEsineenNimi;

    @FXML
    private Label valitunEsineenPaino;
    @FXML
    private Label valitunKorkeus;
    @FXML
    private Label valitunsLeveys;
    @FXML
    private Label valitunSyvyys;
    @FXML
    private VBox toimintaKentta;
    @FXML
    private TabPane toimintaKenttaUusi;
    @FXML
    private ComboBox<Paketti> pakettiValinta;
    @FXML
    private Label matkanPituus;

    /**
     * Initializes the controller class.
     *
     * @param url
     * @param rb
     */
    @Override
    public void initialize(URL url, ResourceBundle rb) {
        kestoIndikaattori.setText("2");
        Mediator.getInstance().registerPaketoija(this);
        paivitaEsineLista();
        paivitaLähettämättömät();
        firstclass.setToggleGroup(group);
        firstclass.setSelected(true);
        secondclass.setToggleGroup(group);
        thirdclass.setToggleGroup(group);

        firstclass.setUserData(Varasto.getInstance().getLuokkaByID(1));
        secondclass.setUserData(Varasto.getInstance().getLuokkaByID(2));
        thirdclass.setUserData(Varasto.getInstance().getLuokkaByID(3));

        group.selectedToggleProperty().addListener(
                (ObservableValue<? extends Toggle> ov, Toggle old_toggle, Toggle new_toggle) -> {
                    Integer numero = Integer.parseInt(group.getSelectedToggle().getUserData().toString());
                    numero *= numero;
                    numero++;
                    kestoIndikaattori.setText(numero.toString());
                    paivitaValittuEsine();
                });

        esineLista.getSelectionModel().selectedItemProperty().addListener(
                (ObservableValue<? extends Esine> ov, Esine t, Esine t1) -> {
                    paivitaValittuEsine();
                });

        päivitäKaupungit();

    }

    private void paivitaEsineLista() {
        DebugLoki.debug("Päivitetään esine lista", 4);
        esineLista.getItems().clear();
        esineLista.getItems().addAll(
                Varasto.getInstance().getKaikkiEsineet());
    }

    private void päivitäKaupungit() {
        DebugLoki.debug("Päivitetään kaupunki lista", 4);
        List<String> kaupungit = TietokantaLuokka.getInstance().getKaupungit();
        lahtoKaupunki.getItems().clear();
        kohdeKaupunki.getItems().clear();

        lahtoKaupunki.getItems().addAll(kaupungit);
        kohdeKaupunki.getItems().addAll(kaupungit);
        päivitäLähtöAutomaatti();
        päivitäKohdeAutomaatti();
    }

    private void päivitäLähtöAutomaatti() {
        DebugLoki.debug("Päivitetään lähtö automaatit", 4);
        lahtoAutomaatti.getItems().clear();
        lahtoAutomaatti.getItems().addAll(Varasto
                .getInstance().getAutomaattiByKaupunki(
                        lahtoKaupunki.getSelectionModel().getSelectedItem()));
    }

    private void päivitäKohdeAutomaatti() {
        DebugLoki.debug("Päivitetään kohde automaatit", 4);
        kohdeAutomaatti.getItems().clear();
        kohdeAutomaatti.getItems().addAll(Varasto
                .getInstance().getAutomaattiByKaupunki(
                        kohdeKaupunki.getSelectionModel().getSelectedItem()));
    }

    private void paivitaValittuEsine() {
        Esine item = esineLista.getSelectionModel().getSelectedItem();
        if (item == null) {
            return;
        }
        DebugLoki.debug("Päivitetään valittu esine", 4);
        valitunEsineenNimi.setText(item.getNimi());
        valitunKorkeus.setText(Integer.toString(item.getKorkeus()));
        valitunsLeveys.setText(Integer.toString(item.getLeveys()));
        valitunSyvyys.setText(Integer.toString(item.getSyvyys()));
        valitunEsineenPaino.setText(Integer.toString(item.getMassa()));

        Luokka luokka = (Luokka) group.getSelectedToggle().getUserData();
        valitunKorkeus.setId("");
        valitunSyvyys.setId("");
        valitunsLeveys.setId("");
        valitunEsineenPaino.setId("");

        if (luokka.getMaxKorkeus() < Integer.parseInt(valitunKorkeus.getText())) {
            valitunKorkeus.setId("labelvaara");
        }
        if (luokka.getMaxLeveys() < Integer.parseInt(valitunsLeveys.getText())) {
            valitunsLeveys.setId("labelvaara");
        }
        if (luokka.getMaxSyvyys() < Integer.parseInt(valitunSyvyys.getText())) {
            valitunSyvyys.setId("labelvaara");
        }
        if (luokka.getPainoraja() < Integer.parseInt(valitunEsineenPaino.getText())) {
            valitunEsineenPaino.setId("labelvaara");
        }
    }

    private void paivitaLähettämättömät() {
        DebugLoki.debug("Päivitetään lähettämättömät", 4);

        pakettiValinta.setItems(Varasto.getInstance().getPaketit());

    }

    @FXML
    private void tarkistaNumero(KeyEvent event) {
        TextField tf = null;
        try {
            tf = (TextField) event.getSource();
            Integer.parseInt(tf.getText());
        } catch (NumberFormatException e) {
            if (tf != null) {
                tf.setText("");
            }
        } catch (ClassCastException ex) {
            DebugLoki.error("Eventin lähde ei ollut text field!!!", ex);
        }
    }

    @FXML
    private void luoEsine(ActionEvent event) {
        Esine uusi;
        try {
            String nimi = esineNimi.getText().replaceAll("\\W*", "");
            if (nimi.isEmpty()) {
                Integer.parseInt("Nimi");
                //heitetään virhe 
            }
            uusi = new Esine(-1, nimi,
                    Integer.parseInt(massa.getText()),
                    Integer.parseInt(korkeus.getText()),
                    Integer.parseInt(leveys.getText()),
                    Integer.parseInt(syvyys.getText()),
                    cBoxIsBreakable.isSelected()
            );
            TietokantaLuokka.getInstance().addEsine(uusi);
            paivitaEsineLista();
            esineLista.getSelectionModel().selectLast();
        } catch (NumberFormatException e) {

            DebugLoki.error("Esineen luonti ei onnistunut", e);
        }

        esineNimi.setText("");
        massa.setText("");
        korkeus.setText("");
        leveys.setText("");
        syvyys.setText("");
    }

    @FXML
    private void handleKeySearch(KeyEvent event) {

        if (event.getCode().isLetterKey()) {
            ComboBox<String> lähde = (ComboBox<String>) event.getSource();

            for (String i : lähde.getItems()) {

                if (i.toLowerCase().startsWith(event.getText().toLowerCase())) {
                    lähde.getSelectionModel().select(i);
                    ((ComboBoxListViewSkin) lähde.getSkin())
                            .getListView().scrollTo(lähde.getSelectionModel()
                                    .getSelectedIndex());
                    lähde.fireEvent(new ActionEvent());
                    break;
                }

            }
        }
    }

    @FXML
    private void infoLuokista(ActionEvent event) {
        DebugLoki.debug("Vaihdetaan näkymää", 4);
        toimintaKenttaUusi.setVisible(!toimintaKenttaUusi.isVisible());
        infoPanel.setVisible(!infoPanel.isVisible());
    }

    @FXML
    private void peruuta(ActionEvent event) {
        ((Node) event.getSource()).getScene().getWindow().hide();
    }

    @FXML
    private void luoPaketti(ActionEvent event) {
        try {
            Toggle selectedToggle = group.getSelectedToggle();
            if (selectedToggle == null) {
                return;
            }
            Luokka luokka = (Luokka) selectedToggle.getUserData();
            Esine item = esineLista.getSelectionModel().getSelectedItem();
            SmartPostAutomaatti lahtoPosti = lahtoAutomaatti
                    .getSelectionModel().getSelectedItem();
            SmartPostAutomaatti kohdePosti = kohdeAutomaatti
                    .getSelectionModel().getSelectedItem();

            if (item == null
                    || luokka == null) {
                DebugLoki.debug("Jokin ei ollut asetettu vielä!", 1);
                avaaDialog("Jokin ei ollut asetettu vielä!", esineLista.getScene().getWindow());
                return;
            }

            Calendar cal = Calendar.getInstance();
            java.sql.Date tanaan = new java.sql.Date(cal.getTime().getTime());

            Paketti p = new Paketti(-1, item, luokka, Mediator.getInstance().getKäyttäjä(), tanaan);
            TietokantaLuokka.getInstance().addPaketti(p);
            avaaDialog("Onnistui \nPaketin tunnistava id: " + p.getPakettiid(), esineLista.getScene().getWindow());
            //infoPanel.getScene().getWindow().hide();
        } catch (IOException ex) {
            DebugLoki.error("Pyydetyn paketin luonti ei onnistunut", ex);
            avaaDialog("Ei onnistunut", esineLista.getScene().getWindow());
        }
        paivitaLähettämättömät();
    }

    public static void avaaDialog(String s, Window omistaja) {

        final Stage dialog = new Stage();
        dialog.initModality(Modality.APPLICATION_MODAL);
        dialog.initOwner(omistaja);

        VBox dialogVbox = new VBox(20);
        dialogVbox.setAlignment(Pos.CENTER);
        dialogVbox.getChildren().add(new Label(s));
        Scene dialogScene = new Scene(dialogVbox, 300, 200);

        dialog.setResizable(false);
        dialog.setScene(dialogScene);
        dialog.showAndWait();

    }

    @FXML
    private void lahetysKaupunkiMuutettu(ActionEvent event) {
        päivitäLähtöAutomaatti();
    }

    @FXML
    private void kohdeKaupunkiMuutettu(ActionEvent event) {
        päivitäKohdeAutomaatti();
    }

    /**
     *
     * @param paketti
     */
    void valitseReitti(Paketti paketti) {
        ((Stage) infoPanel.getScene().getWindow()).show();
        paivitaLähettämättömät();
        infoPanel.getScene().getWindow().requestFocus();
        toimintaKenttaUusi.getSelectionModel().selectLast();
        pakettiValinta.getSelectionModel().select(paketti);
    }

    @FXML
    private void lähetäPaketti(ActionEvent event) {
        SmartPostAutomaatti alku = lahtoAutomaatti.getSelectionModel().getSelectedItem();
        SmartPostAutomaatti kohde = kohdeAutomaatti.getSelectionModel().getSelectedItem();
        Paketti paketti = pakettiValinta.getSelectionModel().getSelectedItem();
        if (alku.equals(kohde)) {
            DebugLoki.debug("Automaatit pitää olla erit!", 2);
            avaaDialog("Automaatit pitää olla erit!", esineLista.getScene().getWindow());
            return;
        }
        if (paketti.getLuokka().getMatkaRaja() < Mediator.getInstance().etäisyys(alku, kohde)) {
            DebugLoki.debug("Liian pitkä matka tälle luokalle!", 2);
            avaaDialog("Liian pitkä matka tälle luokalle!", esineLista.getScene().getWindow());
            return;
        }
        Calendar cal = Calendar.getInstance();
        Date viim = paketti.viimeisenSaapuminen();
        java.sql.Date tanaan;

        tanaan = new java.sql.Date(cal.getTime().getTime());

        if (viim == null || viim.compareTo(tanaan) > 0) {
            tanaan = viim;

        }

        cal.add(Calendar.DAY_OF_YEAR, paketti.getLuokka().getAikaKesto());
        java.sql.Date perilla = new java.sql.Date(cal.getTime().getTime());

        Reitti a = new Reitti(
                paketti.getPakettiid(), paketti.getNextReitti(),
                alku.getId(), kohde.getId(), Mediator.getInstance().etäisyys(alku, kohde),
                tanaan, perilla);

        if (paketti.addReitti(a)) {
            avaaDialog("Reitin lisäys onnistui!", pakettiValinta.getScene().getWindow());
            Mediator.getInstance().piirraKartta(alku, kohde, paketti.getLuokka());
        } else {
            avaaDialog("Reitin lisäys epäonnistui!", pakettiValinta.getScene().getWindow());
        }

        TietokantaLuokka.getInstance().addPaketti(paketti);
        paivitaLähettämättömät();
    }

    @FXML
    private void tarkistaPituus(ActionEvent event) {
        SmartPostAutomaatti lahto = lahtoAutomaatti.getSelectionModel().getSelectedItem();
        SmartPostAutomaatti kohde = kohdeAutomaatti.getSelectionModel().getSelectedItem();
        Paketti paketti = pakettiValinta.getSelectionModel().getSelectedItem();

        if (lahto == null || kohde == null || paketti == null) {
            DebugLoki.debug("Jokin tarvittu tieto matkan laskemiseen ei ollut asetettu", 2);
            matkanPituus.setText("0.00 km");
            return;
        }

        double etäisyys = Mediator.getInstance().etäisyys(lahto, kohde);
        matkanPituus.setText(String.format("%.2f km", etäisyys));

        if (paketti.getLuokka().getMatkaRaja() <= etäisyys) {
            DebugLoki.debug("Matka on liian pitkä tälle luokalle", 3);
            matkanPituus.setId("labelvaara");
        } else {
            matkanPituus.setId("");
        }
    }

}
