/*
 * Tiedosto luotu: Jun 11, 2018
 * 
 * 
 */
package LUT.keskus.automaatit;

import LUT.keskus.DebugLoki;
import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.StringReader;
import java.net.MalformedURLException;
import java.net.URL;
import java.util.ArrayList;
import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;
import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.NodeList;
import org.xml.sax.InputSource;
import org.xml.sax.SAXException;

/**
 *
 * @author Niko Aurelma <niko.aurelma@student.lut.fi>
 */
public class DataBuilder {

    //Singelton
    private static DataBuilder instance;

    /**
     * The single allowed instance
     *
     * @return instance
     */
    public static DataBuilder getInstance() {
        if (instance == null) {
            instance = new DataBuilder();
        }
        return instance;
    }

    private Document doc;
    private final ArrayList<SmartPostAutomaatti> lista;

    private DataBuilder() {
        //http://smartpost.ee/fi_apt.xml

        convert(lataaSivu("http://iseteenindus.smartpost.ee/api/?request=destinations&country=FI&type=APT"));
        lista = new ArrayList<>();
        parsi();

    }

    private String lataaSivu(String nimi) {
        try {
            DebugLoki.debug("Aloitetaan xlm haku", 5);
            URL url = new URL(nimi);
            String total = "";
            try (BufferedReader in = new BufferedReader(
                    new InputStreamReader(url.openStream(), "UTF-8"))) {
                String inputLine;
                while ((inputLine = in.readLine()) != null) {
                    total += inputLine + "\n";
                }
            }
            DebugLoki.debug("XML haettu", 5);
            return total;
        } catch (MalformedURLException ex) {
            DebugLoki.error("Sivun lataamisessa oli virhe", ex);
        } catch (IOException ex) {
            DebugLoki.error("Sivun lataamisessa oli virhe", ex);
        }
        return null;
    }

    private void convert(String total) {
        try {
            DebugLoki.debug("Aloitetaan muutos", 5);
            DocumentBuilderFactory dbFactory
                    = DocumentBuilderFactory.newInstance();
            DocumentBuilder dBuilder = dbFactory.newDocumentBuilder();

            doc = dBuilder.parse(new InputSource(new StringReader(total)));
            doc.getDocumentElement().normalize();
            DebugLoki.debug("Lopetetaan muutos", 5);
        } catch (IOException | ParserConfigurationException | SAXException ex) {
            DebugLoki.error("Virhe XML Muuntamisessa", ex);
        }
    }

    private void parsi() {
        DebugLoki.debug("Aloitetaan parsinta", 5);
        NodeList paikat = doc.getElementsByTagName("item");
        for (int i = 0; i < paikat.getLength(); i++) {
            Element item = (Element) paikat.item(i);
            lista.add(new SmartPostAutomaatti(-1,
                    Integer.parseInt(hae(item, "postalcode")),
                    hae(item, "city"),
                    hae(item, "address"),
                    hae(item, "availability"),
                    hae(item, "name"),
                    Double.parseDouble(hae(item, "lat")),
                    Double.parseDouble(hae(item, "lng"))));

        }
        DebugLoki.debug("Parsinta valmis", 5);
    }

    private String hae(Element e, String nimi) {
        return e.getElementsByTagName(nimi).item(0).getTextContent();
    }

    /**
     * return a list of SmartPosts gotten from the Internet.
     *
     * @return
     */
    public ArrayList<SmartPostAutomaatti> getList() {
        return lista;
    }

}
