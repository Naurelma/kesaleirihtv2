/*
 * Tiedosto luotu: Jun 13, 2018
 * 
 * 
 */
package LUT.gui.ohjaimet;

import LUT.keskus.automaatit.SmartPostAutomaatti;
import LUT.keskus.storage.Luokka;
import LUT.keskus.storage.Paketti;
import LUT.keskus.storage.Reitti;
import java.util.List;

/**
 *
 * @author Niko Aurelma <niko.aurelma@student.lut.fi>
 */
public class Mediator {

    /**
     *
     * @return
     */
    public static Mediator getInstance() {
        return MediatorHolder.instance;
    }

    private KarttaController kartta;
    private ListaajaController listaaja;
    private PaketinLuontiController paketoija;

    private String käyttäjä = "";
    /**
     *
     * @param k
     */
    public void registerKartta(KarttaController k) {
        kartta = k;
    }

    /**
     *
     * @param t
     */
    public void registerListaaja(ListaajaController t) {
        listaaja = t;
    }

    /**
     *
     * @param p
     */
    public void registerPaketoija(PaketinLuontiController p) {
        paketoija = p;
    }

    /**
     *
     * @return
     */
    public SmartPostAutomaatti getValittuAutomaatti() {
        return listaaja.getValittuAutomaatti();
    }

    /**
     *
     * @param alku
     * @param kohde
     * @param luokka
     */
    void piirraKartta(SmartPostAutomaatti alku, SmartPostAutomaatti kohde, Luokka luokka) {
        kartta.piirraKartta(alku, kohde, luokka);
    }

    /**
     *
     * @param paketti
     */
    void valitseReitti(Paketti paketti) {
        paketoija.valitseReitti(paketti);
    }

    void piirräMerkki(SmartPostAutomaatti automat) {
        kartta.lisaaUusiMerkki(automat);
    }

    void piirräReitit(List<Reitti> rl, int nopeus) {
        kartta.piirräReitit(rl, nopeus);
    }

    /**
     *
     * @param alku
     * @param loppu
     * @return
     */
    public double etäisyys(SmartPostAutomaatti alku, SmartPostAutomaatti loppu) {
        return kartta.laskeEtäisyys(alku, loppu);
    }

    

    /**
     * Get the value of käyttäjä
     *
     * @return the value of käyttäjä
     */
    public String getKäyttäjä() {
        return käyttäjä;
    }

    /**
     * Set the value of käyttäjä
     *
     * @param käyttäjä new value of käyttäjä
     */
    public void setKäyttäjä(String käyttäjä) {
        this.käyttäjä = käyttäjä;
    }

    private static class MediatorHolder {

        private static Mediator instance = new Mediator();

        private MediatorHolder() {
        }

    }

}
