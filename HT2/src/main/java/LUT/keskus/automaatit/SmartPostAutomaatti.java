/*
 * Tiedosto luotu: Jun 11, 2018
 * 
 * 
 */
package LUT.keskus.automaatit;

import java.util.Locale;

/**
 *
 * @author Niko Aurelma <niko.aurelma@student.lut.fi>
 */
public class SmartPostAutomaatti {

    private final int id;
    private final int postinumero;
    private final String kaupunki;
    private final String osoite;
    private final String aukioloajat;
    private final String postoffice;

    private final GeoPoint sijainti;

    /**
     *
     * @param id
     * @param postinumero
     * @param kaupunki
     * @param osoite
     * @param aukiOloAjat
     * @param office
     * @param lat
     * @param lng
     */
    public SmartPostAutomaatti(int id,
            int postinumero, String kaupunki,
            String osoite, String aukiOloAjat,
            String office, double lat, double lng) {

        this.id = id;
        this.postinumero = postinumero;
        this.kaupunki = kaupunki;
        this.osoite = osoite;
        this.aukioloajat = aukiOloAjat;
        this.postoffice = office;
        this.sijainti = new GeoPoint(lat, lng);

    }

    /**
     *
     * @return
     */
    public int getId() {
        return id;
    }

    /**
     *
     * @return
     */
    public int getPostinumero() {
        return postinumero;
    }

    /**
     *
     * @return
     */
    public String getKaupunki() {
        return kaupunki;
    }

    /**
     *
     * @return
     */
    public String getOsoite() {
        return osoite;
    }

    /**
     *
     * @return
     */
    public String getAukioloajat() {
        return aukioloajat;
    }

    /**
     *
     * @return
     */
    public String getPostoffice() {
        return postoffice;
    }

    /**
     *
     * @return
     */
    public GeoPoint getSijainti() {
        return sijainti;
    }

    /**
     *
     * @return
     */
    public String esita() {
        return "SmartPostAutomaatti{" + "id=" + id + ", postinumero=" + postinumero + ", kaupunki=" + kaupunki + ", osoite=" + osoite + ", aukioloajat=" + aukioloajat + ", postoffice=" + postoffice + ", sijainti=" + sijainti + '}';
    }

    @Override
    public String toString() {
        return postoffice;
    }

    /**
     *
     * @author Niko Aurelma <niko.aurelma@student.lut.fi>
     */
    public class GeoPoint {

        private final double lat;
        private final double lng;

        /**
         *
         * @param lat
         * @param lng
         */
        public GeoPoint(double lat, double lng) {
            super();
            this.lat = lat;
            this.lng = lng;
        }

        /**
         *
         * @return
         */
        public double getLat() {
            return lat;
        }

        /**
         *
         * @return
         */
        public double getLng() {
            return lng;
        }

        /**
         * Make sure to not have "," in the number
         *
         * @return
         */
        public String getLatStr() {
            return String.format(Locale.UK, "%f", lat);
        }

        /**
         * Make sure to not have "," in the number
         *
         * @return
         */
        public String getLngStr() {
            return String.format(Locale.UK, "%f", lng);
        }

        @Override
        public String toString() {
            return "GeoPoint{" + "lat=" + lat + ", lng=" + lng + '}';
        }
    }

}
