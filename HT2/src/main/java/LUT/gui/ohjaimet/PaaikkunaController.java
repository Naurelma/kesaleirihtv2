/*
 * Tiedosto luotu: Jun 7, 2018
 * 
 * 
 */
package LUT.gui.ohjaimet;

import LUT.keskus.DebugLoki;
import LUT.tietokanta.TietokantaLuokka;

import java.io.IOException;
import java.net.URL;
import java.nio.charset.StandardCharsets;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.util.ResourceBundle;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.fxml.Initializable;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.control.CheckMenuItem;
import javafx.scene.control.Menu;
import javafx.scene.control.MenuItem;
import javafx.scene.control.PasswordField;
import javafx.scene.control.TextField;
import javafx.scene.layout.HBox;
import javafx.scene.layout.VBox;
import javafx.stage.Stage;
import javax.xml.bind.DatatypeConverter;

/**
 * FXML Controller class
 *
 * @author Niko Aurelma <niko.aurelma@student.lut.fi>
 */
public class PaaikkunaController implements Initializable {

    private final Stage kartta = null;
    private final Stage listaaja = null;
    private final Stage paketinLuonti = null;
    private String käyttäjä = "";
    private String ssHash = "";

    @FXML
    private Menu ikkunatMenu;
    @FXML
    private VBox paaikkuna;
    @FXML
    private HBox kirjautuminen;
    @FXML
    private TextField tunnus;
    @FXML
    private PasswordField salasana;

    /**
     * Initializes the controller class.
     *
     * @param url
     * @param rb
     */
    @Override
    public void initialize(URL url, ResourceBundle rb) {
        // TODO
        TietokantaLuokka.getInstance();
        //Varmistetaan sen olemasssa olo

    }

    private void luoIkkuna(Stage avattava, String nimi, boolean nayta) {

        if (avattava == null) {
            try {
                Parent root = FXMLLoader.load(getClass()
                        .getResource(String.format("/fxml/%s.fxml", nimi)));
                avattava = new Stage();
                avattava.setTitle(nimi);
                avattava.setScene(new Scene(root, 1600, 900));

            } catch (IOException e) {
                DebugLoki.error("Ikkunan (" + nimi + ") avaamisessa oli ongelma", e);

            }
        }
        if (avattava == null) {
            DebugLoki.debug("Avattava ikkuna on null", 3);
        } else if (nayta) {
            avattava.show();
            avattava.requestFocus();
        }
    }
    private void alusta(){
        luoIkkuna(kartta, "kartta", false);
        luoIkkuna(listaaja, "listaaja", false);
        luoIkkuna(paketinLuonti, "paketinLuonti", false);
    }

    @FXML
    private void handleKartta(ActionEvent event) {
        luoIkkuna(kartta, "kartta", true);

    }

    @FXML
    private void handleListaaja(ActionEvent event) {
        luoIkkuna(listaaja, "listaaja", true);
    }

    @FXML
    private void handleLuonti(ActionEvent event) {
        luoIkkuna(paketinLuonti, "paketinLuonti", true);
    }

    @FXML
    private void handleReset(ActionEvent event) {
        if (((CheckMenuItem) event.getSource()).isSelected()) {
            DebugLoki.debug("Menu on valittuna" + event.getSource().toString(), 3);

            ((MenuItem) event.getSource()).setText("TYHJENNÄ TIETOKANTA");

        } else {
            DebugLoki.debug("Menu ei ole valittuna:" + event.getSource().toString(), 3);

            ((MenuItem) event.getSource()).setText("Prime");

            TietokantaLuokka.getInstance().reset();
        }
    }

    @FXML
    private void avaaKaikki(ActionEvent event) {
        for (MenuItem item : ikkunatMenu.getItems()) {
            item.fire();
        }
    }

    private boolean tarkistaTiedot() {
        try {

            käyttäjä = tunnus.getText().replaceAll("\\W*", "");
            tunnus.setText(käyttäjä);
            String ss = salasana.getText();
            salasana.setText("");

            if (käyttäjä.isEmpty() || ss.isEmpty()) {
                DebugLoki.debug("Jompikumpi oli tyhjä:" + käyttäjä + "::" + ss, 0);
                return false;
            }
            ss = käyttäjä + "::" + ss;

            ssHash = DatatypeConverter.printHexBinary(
                    MessageDigest.getInstance("SHA-256")
                            .digest(ss.getBytes(StandardCharsets.UTF_8)));
            DebugLoki.debug("Tiedot:", 2);
            DebugLoki.debug(käyttäjä, 2);
            DebugLoki.debug(ssHash, 2);
            
            return true;
        } catch (NoSuchAlgorithmException ex) {
            DebugLoki.error("Algoritmia ei löytynyt!", ex);
        }
        return false;
    }

    @FXML
    private void kirjaudu(ActionEvent event) {

        if (tarkistaTiedot() && TietokantaLuokka.getInstance()
                .tarkistaKirjautuminen(käyttäjä, ssHash)) {
            kirjautuminen.setVisible(false);
            paaikkuna.setVisible(true);
            Mediator.getInstance().setKäyttäjä(käyttäjä);
            alusta();
        }

    }

    @FXML
    private void luoTunnus(ActionEvent event) {

        if (tarkistaTiedot() && TietokantaLuokka.getInstance().addTunnus(käyttäjä, ssHash)) {

            kirjautuminen.setVisible(false);
            paaikkuna.setVisible(true);
            Mediator.getInstance().setKäyttäjä(käyttäjä);
            alusta();
        }

    }

}
