/*
 * Tiedosto luotu: Jun 11, 2018
 * 
 * 
 */
package LUT.keskus.storage;

import LUT.keskus.DebugLoki;
import java.io.IOException;
import java.sql.Date;
import java.util.ArrayList;
import java.util.List;

/**
 *
 * @author Niko Aurelma <niko.aurelma@student.lut.fi>
 */
public class Paketti {

    private int pakettiid;

    private Esine sisalto;
    private Luokka luokka;
    private Date luomispaiva;
    private List<Reitti> kuljettuReitti;
    private String omistaja;

    /**
     *
     * @param id
     * @param s
     * @param l
     * @param omistaja
     * @param luomisPäivä
     * @throws IOException
     */
    public Paketti(
            int id, Esine s,
            Luokka l, String omistaja, Date luomisPäivä) throws IOException {
        if (!l.canContain(s)) {
            throw new IOException("Annettu esine ei sovi luokkaan");
        }
        DebugLoki.debug(String.format("Luodaan uusi paketti:%d, %d, %d", id, s.getId(), l.getNro()), 10);
        this.pakettiid = id;
        sisalto = s;
        luokka = l;
        this.luomispaiva = luomisPäivä;
        kuljettuReitti = new ArrayList<>();
        this.omistaja = omistaja;
        

    }

    /**
     * Get the value of omistaja
     *
     * @return the value of omistaja
     */
    public String getOmistaja() {
        return omistaja;
    }

    /**
     *
     * @return
     */
    public int getPakettiid() {
        return pakettiid;
    }

    /**
     *
     * @return
     */
    public Esine getSisalto() {
        return sisalto;
    }

    /**
     *
     * @return
     */
    public Luokka getLuokka() {
        return luokka;
    }

    /**
     *
     * @param pakettiid
     */
    public void setPakettiid(int pakettiid) {
        this.pakettiid = pakettiid;
    }

    public boolean addReitti(Reitti r) {
        if (kuljettuReitti.isEmpty()
                || (r.getLahetysaika().compareTo(viimeisenSaapuminen()) > 0)) {
            kuljettuReitti.add(r);
            DebugLoki.debug("Onnistui", 10);
            return true;
        }
        DebugLoki.debug("Reitin lisäys ei onnistunut ", 10);
        DebugLoki.debug(esitä(), 10);
        DebugLoki.debug(r.esitä(), 10);

        return false;
    }

    public Date viimeisenSaapuminen() {
        if (kuljettuReitti.isEmpty()) {
            return null;
        }
        return kuljettuReitti.get(kuljettuReitti.size() - 1)
                .getSaapumisaika();
    }

    public Date getLuomispaiva() {
        return luomispaiva;
    }

    public boolean paivita(Luokka l, Esine e) {
        if (l.canContain(e)) {
            this.luokka = l;
            this.sisalto = e;
            return true;
        }
        return false;
    }

    @Override
    public String toString() {
        return "Paketti{" + "id=" + pakettiid + ", sisalto=" + sisalto.getNimi()
                + ", luokka=" + luokka.getNimi() +", omistaja=" +omistaja+'}';
    }

    public List<Reitti> getKuljettuReitti() {
        return kuljettuReitti;
    }

    /**
     *
     * @return
     */
    public String esitä() {
        return "Paketti{" + "id=" + pakettiid
                + ", sis\u00e4lt\u00f6=" + sisalto.esita()
                + ", luokka=" + luokka.esita() + ", ReittienLKM=" + kuljettuReitti.size() + ", Reitit=[" + reitti() + "]}";
    }

    private String reitti() {
        String s = "";
        for (Reitti r : kuljettuReitti) {
            s += r.esitä() + ",";
        }
        return s;
    }

    public int getNextReitti() {
        return kuljettuReitti.size();
    }

}
